const express = require('express');
const bodyParser = require('body-parser');
const cors=require('cors')
const jwt=require('jsonwebtoken')
const video = require('./routes/video')
 
const PORT=300;
const api=require('./routes/api')

const app= express();
app.use(cors())
app.use(bodyParser.json());

app.use('/api',api)
app.use('/video',video)

app.post('/',(req,res)=>{
console.log(req)
    res.status(200).send(req.rawHeaders);
})

app.listen(PORT,()=>{

console.log('listening'+300);

})