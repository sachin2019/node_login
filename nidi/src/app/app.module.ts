import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http'
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';
import { SpecialEventsComponent } from './special-events/special-events.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './auth.service';
import{ FormsModule} from '@angular/forms'
import { EventService } from './event.service';
import { AuthGuard } from './auth.guard';
import { TokenIntercpService } from './token-intercp.service';
import { FormsComponent } from './forms/forms.component';
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    EventsComponent,
    SpecialEventsComponent,
    FormsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ AuthService,EventService,AuthGuard,{provide:HTTP_INTERCEPTORS,useClass:TokenIntercpService,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
