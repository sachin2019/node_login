import { NgModule } from '@angular/core';
import { Routes , RouterModule} from '@angular/router';
import { EventsComponent } from './events/events.component';
import { SpecialEventsComponent } from './special-events/special-events.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './auth.guard';
import { FormsComponent } from './forms/forms.component';



//route guard for entire application

// const routes: Routes = [{path: '', canActivate:[AuthGuard] ,children: [
//   {
//     path: '',
//     redirectTo:'events',
//     pathMatch: 'full'
//   },

//   {
//     path: 'events',
//     component: EventsComponent
//   },
//   {
//     path: 'specialEvents',
//     component: SpecialEventsComponent,
//     canActivate:[AuthGuard]
  

//   }

  
// ]}
// , {
//   path: 'login',
//   component: LoginComponent
// }
// , {
//   path: 'register',
//   component: RegisterComponent
// }
// ];
/////////////////////////////////////////////

 const routes: Routes = [
  {
    path: '',
    redirectTo:'events',
    pathMatch: 'full'
  },

  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: 'specialEvents',
    component: SpecialEventsComponent,
    canActivate:[AuthGuard]
  

  }

  

, {
  path: 'login',
  component: LoginComponent
}
, {
  path: 'register',
  component: RegisterComponent
}
,{
  path: 'forms',
  component: FormsComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
