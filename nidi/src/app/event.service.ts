import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class EventService {
url="http://localhost:300/api/"
  constructor(private http:HttpClient) { }

  eventss(){
    return this.http.get<any>(this.url+'eventList');
  }

  specialEvents(){

    return this.http.get<any>(this.url+'specialtList');
  }


}
