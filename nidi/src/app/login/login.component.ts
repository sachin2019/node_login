import { Component, OnInit } from '@angular/core';
import{ AuthService } from '../auth.service'
import {  Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registerUserData={}
  
  constructor( private auth:AuthService,private _route:Router) { }

  ngOnInit() {
  }

  login(){
    console.log(this.registerUserData)

    this.auth.login(this.registerUserData).subscribe(
      res=>{
        console.log(res);
        localStorage.setItem('token',res.token);
        this._route.navigate(['/specialEvents'])
      
      },err=>{console.log(err)}
    )
  }

}