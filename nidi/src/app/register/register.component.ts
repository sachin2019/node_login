import { Component, OnInit } from '@angular/core';
import{ AuthService } from '../auth.service';
import { Router} from '@angular/router'
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
registerUserDat={}
  constructor( private auth:AuthService,private route:Router) { }

  ngOnInit() {
  }

  Register(){
    console.log(this.registerUserDat)

    this.auth.registeRr(this.registerUserDat).subscribe(
      res=>{
        console.log(res);
      localStorage.setItem('token',res.token);
      this.route.navigate(['/login'])
      },err=>{console.log(err)}
    )
  }

}
