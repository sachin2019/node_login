import { Component, OnInit } from '@angular/core';

import { EventService } from '../event.service'
@Component({
  selector: 'app-special-events',
  templateUrl: './special-events.component.html',
  styleUrls: ['./special-events.component.css']
})
export class SpecialEventsComponent implements OnInit {
  eventt:string[]=[]

  constructor( private service:EventService) { }

  ngOnInit() {
    this.event()
  }
  event(){
    this.service.specialEvents().subscribe(
      res=>{this.eventt=res},
      err=>{console.log(err)}
  
    )
  }
}
