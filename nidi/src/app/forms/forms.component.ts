import { Component, OnInit } from '@angular/core'
import { FormsService } from './forms.service' 


@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
 ansible:any={}
 conf_directories=[]
 vol_mounts=['a','b','c','d']
 volumeString:string="/var/www/website"
  constructor(private _service:FormsService) { }

  ngOnInit() {
   
  }

  ansibleSubmit(){

let a = this.conf_directories.length
let i=0
this.conf_directories.forEach(element => {

  if(a!=i){

    let middle:string=element.split('/')
    this.vol_mounts[i]=this.ansible.target_website_directory+"/"+middle[middle.length-1]+":"+this.conf_directories[i]

  }
  i++;
  

});
this.vol_mounts[(this.vol_mounts.length-1)+1]=this.ansible.source_website_directory+':'+this.ansible.target_website_directory

this.vol_mounts[(this.vol_mounts.length-1)+1]="/var/log/httpd/"+this.ansible.virtual_host+':/var/log/httpd'

    
    this.ansible.conf_directories=this.conf_directories
    this.ansible.volume_mounts=this.vol_mounts
   
this._service.ansible(this.ansible)
.subscribe(res=>{
console.log('res')

})

}

  }


